-- Full SQL code


/* Drop Views */

DROP VIEW T155042_Akiivne_V_Mitte_Laud
;
DROP VIEW T155042_Koik_lauad 
;
DROP VIEW T155042_Laudade_Detailid
;
DROP VIEW T155042_Laudade_kat_omamine
;
DROP VIEW T155042_Laudade_koondaruanne
;

/* Drop Tables */

DROP TABLE t155042_Töötaja_seis_liik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Riik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laua_tüüp CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laua_seisundi_liik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laua_kategooria_tüüp CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laua_kategooria CASCADE CONSTRAINTS
;

DROP TABLE t155042_Kliendi_seisundi_liik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Isiku_seisundi_liik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Amet CASCADE CONSTRAINTS
;

DROP TABLE t155042_Isik CASCADE CONSTRAINTS
;

DROP TABLE t155042_Klient CASCADE CONSTRAINTS
;

DROP TABLE t155042_Töötaja CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laud CASCADE CONSTRAINTS
;

DROP TABLE t155042_Laua_kat_oma CASCADE CONSTRAINTS
;




/* Create Tables */

CREATE TABLE  t155042_Amet
(
	Amet_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	Kirjeldus CLOB NULL,
	CONSTRAINT t155042_PK_AMET PRIMARY KEY (Amet_kood),
	CONSTRAINT t155042_AK_AMET_NIMETUS UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_AMET_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_KIRJELDUS_NE CHECK (NOT REGEXP_LIKE(kirjeldus,'^[[:space:]]*$'))
)
;



CREATE TABLE  t155042_Isiku_seisundi_liik
(
	Isiku_seisundi_liik_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_ISIKU_SEISUNDI_LIIK PRIMARY KEY (Isiku_seisundi_liik_kood),
	CONSTRAINT t155042_AK_ISIKU_SEIS_LIIK_NIM UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_ISL_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;


CREATE TABLE  t155042_Kliendi_seisundi_liik
(
	Kliendi_seisundi_liik_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR(50) NOT NULL,
	CONSTRAINT t155042_PK_KLIENDI_SEIS_LIIK PRIMARY KEY (Kliendi_seisundi_liik_kood),
	CONSTRAINT t155042_AK_KLIENDI_SEIS_L_NIM UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_KSL_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;

CREATE TABLE  t155042_Laua_kategooria_tüüp
(
	Laua_kategooria_tüüp_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_LAUA_KAT_TÜÜP PRIMARY KEY (Laua_kategooria_tüüp_kood),
	CONSTRAINT t155042_AK_LAUA_KAT_TÜÜP_NIM UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_LKT_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;



CREATE TABLE  t155042_Laua_seisundi_liik
(
	Laua_seisundi_liik_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_LAUA_SEISUNDI_LIIK PRIMARY KEY (Laua_seisundi_liik_kood),
	CONSTRAINT t155042_AK_LAUA_SEIS_LIIK_NIM UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_LS_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;

CREATE TABLE  t155042_Laua_tüüp
(
	Laua_tüüp_kood NUMBER(3) NOT NULL,
	Kohtade_arv NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_LAUA_TÜÜP PRIMARY KEY (Laua_tüüp_kood),
	CONSTRAINT t155042_AK_LAUA_TÜÜP_NIMETUS UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_KOHTADE_ARV_N_NEG CHECK (kohtade_arv>0),
	CONSTRAINT t155042_CHK_LAUA_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;


CREATE TABLE  t155042_Riik
(
	Riik_kood CHAR(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_RIIK PRIMARY KEY (Riik_kood),
	CONSTRAINT t155042_AK_RIIK_NIMETUS UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_RIIK_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_RIIK_NE CHECK (NOT REGEXP_LIKE(Riik_kood,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_RIIK_KOOD CHECK (REGEXP_LIKE(Riik_kood, '^[A-Z]{3}$'))
)
;

CREATE TABLE  t155042_Töötaja_seis_liik
(
	Töötaja_seisundi_liik_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_TÖÖTAJA_SEIS_LIIK PRIMARY KEY (Töötaja_seisundi_liik_kood),
	CONSTRAINT t155042_AK_TÖÖTAJA_SEIS_L_N UNIQUE (Nimetus),
	CONSTRAINT t155042_CHK_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$'))
)
;

CREATE TABLE  t155042_Isik
(
	"ISIK_ID" NUMBER(8,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
	isikukoodi_riik CHAR(3) NOT NULL,
	Isikukood VARCHAR2(50) NOT NULL,
	E_meil VARCHAR2(254) NOT NULL,
	Parool VARCHAR2(128) NOT NULL,
	SOOL VARCHAR2(32) NOT NULL,
	Isiku_seisundi_liik_kood NUMBER(3) DEFAULT 1 NOT NULL,
	Sünni_kp DATE NOT NULL,    -- muuta tüübiks timestamp?
	Reg_aeg DATE DEFAULT Localtimestamp(0) NOT NULL,
	Eesnimi VARCHAR2(1000) NOT NULL,
	Perenimi VARCHAR2(1000) NULL,
	Elukoht VARCHAR2(500) NULL,    -- Elukoht VARCHAR2(500 None) NULL, remove none
	CONSTRAINT t155042_PK_ISIK PRIMARY KEY (Isik_ID),
	CONSTRAINT t155042_AK_ISIK_IK_IK_RIIK UNIQUE (Isikukood,isikukoodi_riik),
	CONSTRAINT t155042_AK_ISIK_E_MEIL UNIQUE (E_meil),
	CONSTRAINT t155042_CHK_ELUKOHT_NE_ND CHECK (NOT REGEXP_LIKE(Elukoht,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_EESNIMI_NE CHECK (NOT REGEXP_LIKE(eesnimi,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_REG_AEG CHECK (reg_aeg BETWEEN to_date('2010-01-01','YYYY-MM-DD') AND to_date('2100-01-01 23:59:59','YYYY-MM-DD HH24:MI:SS')),
	CONSTRAINT t155042_CHK_SÜNNI_KP CHECK (sünni_kp BETWEEN to_date('1900-01-01','YYYY-MM-DD') AND to_date('2100-01-01 23:59:59','YYYY-MM-DD HH24:MI:SS')
	),
	CONSTRAINT t155042_CHK_EMEIL CHECK (REGEXP_LIKE(e_meil, '@{1}', 'i')),
	CONSTRAINT t155042_CHK_ISIKUKOOD CHECK (NOT REGEXP_LIKE(isikukood,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_SÜNNI_KP_AINULT CHECK (trunc(Sünni_kp)=Sünni_kp),
	CONSTRAINT t155042_CHK_PERENIMI_NE CHECK (NOT REGEXP_LIKE(perenimi,'^[[:space:]]*$')),
	CONSTRAINT t155042_CHK_REG_AEG_NOT_KP CHECK (reg_aeg >= sünni_kp ),
	CONSTRAINT t155042_FK_ISIK_ISIKU_SEIS_L FOREIGN KEY (Isiku_seisundi_liik_kood) REFERENCES  t155042_Isiku_seisundi_liik (Isiku_seisundi_liik_kood),
	CONSTRAINT t155042_FK_ISIK_RIIK FOREIGN KEY (isikukoodi_riik) REFERENCES  t155042_Riik (Riik_kood)
)
;

CREATE TABLE  t155042_Klient
(
	Klient_kood NUMBER(8) NOT NULL,
	Kliendi_seisundi_liik_kood NUMBER(3) DEFAULT 1 NOT NULL,
	On_nõus_tülitamisega NUMBER(1) DEFAULT 0 NOT NULL,    -- Tulenevalt arireeglitest peab iga kliendi korral saama registreerida, kas ta on nous oma tarbijaharjumuste uurimisega voi otseturundusega voi mitte. Vaikimisi andmekaitse pohimottest lahtuvalt tuleb vaikimisi eeldada, et klient ei ole sellega nous.  Tuleb lisada eeldus ,et saab olla kas 0 või 1 
	CONSTRAINT t155042_PK_KLIENT PRIMARY KEY (Klient_kood),
	CONSTRAINT t155042_CHK_NÕUS_TYLITAMISEGA CHECK (On_nõus_tülitamisega IN (0, 1)),
	CONSTRAINT t155042_FK_KLIENT_K_S_L FOREIGN KEY (Kliendi_seisundi_liik_kood) REFERENCES  t155042_Kliendi_seisundi_liik (Kliendi_seisundi_liik_kood),
	CONSTRAINT t155042_FK_KLIENT_ISIK FOREIGN KEY (Klient_kood) REFERENCES  t155042_Isik (Isik_ID) ON DELETE Cascade
)
;

CREATE TABLE  t155042_Laua_kategooria
(
	Laua_kategooria_kood NUMBER(3) NOT NULL,
	Laua_kategooria_tüüp_kood NUMBER(3) NOT NULL,
	Nimetus VARCHAR2(50) NOT NULL,
	CONSTRAINT t155042_PK_LAUA_KATEGOORIA PRIMARY KEY (Laua_kategooria_kood),
	CONSTRAINT t155042_AK_LAUA_KATEGOORIA UNIQUE (Nimetus,Laua_kategooria_tüüp_kood),
	CONSTRAINT t155042_CHK_LKG_NIMETUS_NE CHECK (NOT REGEXP_LIKE(nimetus,'^[[:space:]]*$')),
	CONSTRAINT t155042_FK_LAUA_KAT_L_K_T FOREIGN KEY (Laua_kategooria_tüüp_kood) REFERENCES  t155042_Laua_kategooria_tüüp (Laua_kategooria_tüüp_kood)
)
;
CREATE TABLE  t155042_Töötaja
(
	TÖÖTAJA_ID NUMBER(8) NOT NULL,
	Amet_kood NUMBER(3) NOT NULL,
	Töötaja_seisundi_liik_kood NUMBER(3) DEFAULT 1 NOT NULL,
	mentor NUMBER(8) NULL,
	CONSTRAINT t155042_PK_TÖÖTAJA PRIMARY KEY (TÖÖTAJA_ID),
	CONSTRAINT t155042_CHK_MENTOR_TÖÖTAJA CHECK (mentor<>töötaja_id),
	CONSTRAINT t155042_FK_TÖÖTAJA_AMET FOREIGN KEY (Amet_kood) REFERENCES  t155042_Amet (Amet_kood),
	CONSTRAINT t155042_FK_TÖÖTAJA_T_S_L FOREIGN KEY (Töötaja_seisundi_liik_kood) REFERENCES  t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood),
	CONSTRAINT t155042_FK_TÖÖTAJA_ISIK FOREIGN KEY (TÖÖTAJA_ID) REFERENCES  t155042_Isik (Isik_ID) ON DELETE Cascade,
	CONSTRAINT t155042_FK_TÖÖTAJA_MENTOR FOREIGN KEY (mentor) REFERENCES  t155042_Töötaja (TÖÖTAJA_ID) ON DELETE Set Null
)
;

CREATE TABLE  t155042_Laud
(
	Laud_kood NUMBER(8) NOT NULL,
	Reg_aeg DATE DEFAULT Localtimestamp(0) NOT NULL,
	Kommentaar CLOB NULL,
	Laua_seisundi_liik_kood NUMBER(3) DEFAULT 1 NOT NULL,
	Laua_tüüp_kood NUMBER(3) NOT NULL,
	Registreerija_kood NUMBER(3) NOT NULL,
	CONSTRAINT t155042_PK_LAUD PRIMARY KEY (Laud_kood),
	CONSTRAINT t155042_CHK_LAUD_REG_AEG CHECK (Reg_aeg BETWEEN to_date('2010-01-01','YYYY-MM-DD') AND to_date('2100-01-01 23:59:59','YYYY-MM-DD HH24:MI:SS')),
	CONSTRAINT t155042_CHK_LAUD_K_EI_K_TYHIK CHECK (NOT REGEXP_LIKE(Kommentaar,'^[[:space:]]*$')),
	CONSTRAINT t155042_FK_LAUD_L_S_L FOREIGN KEY (Laua_seisundi_liik_kood) REFERENCES  t155042_Laua_seisundi_liik (Laua_seisundi_liik_kood),
	CONSTRAINT t155042_FK_LAUD_LAUA_TÜÜP FOREIGN KEY (Laua_tüüp_kood) REFERENCES  t155042_Laua_tüüp (Laua_tüüp_kood),
	CONSTRAINT t155042_FK_LAUD_REGISTREERIB FOREIGN KEY (Registreerija_kood) REFERENCES  t155042_Töötaja (TÖÖTAJA_ID)
)
;

CREATE TABLE  t155042_Laua_kat_oma
(
	Laud_kood NUMBER(8) NOT NULL,
	Laua_kategooria_kood NUMBER(3) NOT NULL,
	CONSTRAINT t155042_PK PRIMARY KEY (Laud_kood,Laua_kategooria_kood),
	CONSTRAINT t155042_FK_LAUA_KAT_OMA_LAUD FOREIGN KEY (Laud_kood) REFERENCES  t155042_Laud (Laud_kood) ON DELETE Cascade,
	CONSTRAINT t155042_FK_LAUA_KAT_OMA_L_KAT FOREIGN KEY (Laua_kategooria_kood) REFERENCES  t155042_Laua_kategooria (Laua_kategooria_kood)
)
;


/* Create INDEXS */



CREATE INDEX t155042_IXFK_ISIK_ISIKU_SEIS_L   
 ON  t155042_Isik (Isiku_seisundi_liik_kood) 
;
CREATE INDEX t155042_IXFK_ISIK_RIIK   
 ON  t155042_Isik (isikukoodi_riik) 
;

CREATE INDEX t155042_IXFK_KLIENT_K_S_L   
 ON  t155042_Klient (Kliendi_seisundi_liik_kood) 
;

CREATE INDEX t155042_IXFK_LAUA_KAT_OMA_L_K   
 ON  t155042_Laua_kat_oma (Laua_kategooria_kood) 
;


CREATE INDEX t155042_IXFK_LAUA_KAT_L_K_T   
 ON  t155042_Laua_kategooria (Laua_kategooria_tüüp_kood) 
;
CREATE INDEX t155042_IXFK_LAUD_REGISTREERIB   
 ON  t155042_Laud (Registreerija_kood) 
;
CREATE INDEX t155042_IXFK_LAUD_L_S_L   
 ON  t155042_Laud (Laua_seisundi_liik_kood) 
;
CREATE INDEX t155042_IXFK_LAUD_LAUA_TÜÜP   
 ON  t155042_Laud (Laua_tüüp_kood) 
;


CREATE INDEX IXt155042_FK_TÖÖTAJA_MENTOR   
 ON  t155042_Töötaja (mentor) 
;
CREATE INDEX t155042_IXFK_TÖÖTAJA_AMET   
 ON  t155042_Töötaja (Amet_kood) 
;

CREATE INDEX t155042_IXFK_TÖÖTAJA_T_S_L   
 ON  t155042_Töötaja (Töötaja_seisundi_liik_kood) 
;


/* Insert Data from xml */

--SELECT * FROM c##naited.Riik_xml;
INSERT INTO t155042_Riik (riik_kood, nimetus)
SELECT x.riik_kood, x.nimetus
FROM c##naited.Riik_xml r,
xmltable('//countries/country' PASSING r.SYS_NC_ROWINFO$
COLUMNS riik_kood VARCHAR2(3) PATH 'Alpha-3_code',
nimetus VARCHAR2(100) PATH 'English_short_name_lower_case') x;
/*Laadin andmed tabelisse Riik. XML formaadis andmete tabeli
kujule teisendamiseks kasutatakse XMLTABLE funktsiooni. See
funktsioon on osa XQuery päringukeelest, mis on mõeldud XML
formaadis andmete käitlemiseks.*/

INSERT INTO t155042_ISIKU_SEISUNDI_LIIK (ISIKU_SEISUNDI_LIIK_KOOD, nimetus) VALUES
(1,'Elus');
/*Tuleb lisada FK kirje ISIK tabeli jaoks*/


INSERT INTO t155042_Isik(isikukoodi_riik, isikukood, eesnimi,
perenimi, e_meil, sünni_kp, isiku_seisundi_liik_kood, sool,
parool, elukoht)
SELECT riigid.isikukoodi_riik, isikud.isikukood, isikud.eesnimi,
isikud.perenimi, isikud.e_meil, isikud.sünni_kp,
isikud.isiku_seisundi_liik_kood, 'puudub', isikud.parool,
isikud.elukoht
FROM c##naited.Isik_xml,
xmltable ('*/riik'
 PASSING Isik_xml.SYS_NC_ROWINFO$
 COLUMNS isikukoodi_riik CHAR(3) PATH 'riigi_kood',
isikud xmltype PATH 'isikud'
) riigid,
xmltable ('*/isik'
 PASSING riigid.isikud
 COLUMNS eesnimi VARCHAR2(1000) PATH 'eesnimi',
perenimi VARCHAR2(1000) PATH 'perekonnanimi',
elukoht VARCHAR2(1000) PATH 'aadress',
e_meil VARCHAR2(254) PATH 'email',
isikukood VARCHAR2(100) PATH 'isikukood',
sünni_kp DATE PATH 'synni_aeg',
parool VARCHAR2(100) PATH 'parool',
isiku_seisundi_liik_kood NUMBER PATH 'seisund'
) isikud
WHERE isiku_seisundi_liik_kood=1;

/*Insert data manually */


INSERT INTO t155042_Amet (amet_kood, nimetus , kirjeldus) VALUES (1,'Juhataja', 'Asutuse juht ,kes juhib restorani ketti');
INSERT INTO t155042_Amet (amet_kood, nimetus , kirjeldus) VALUES (2, 'Laudade haldur', ' Teeb kõike seoses laudadega');
INSERT INTO t155042_Amet (amet_kood, nimetus , kirjeldus) VALUES (3, 'Raamatupidaja', ' Hoiab paberid korras');

INSERT INTO t155042_Isiku_seisundi_liik (Isiku_seisundi_liik_kood, nimetus) VALUES (2,'Surnud');

INSERT INTO t155042_Kliendi_seisundi_liik (Kliendi_seisundi_liik_kood, nimetus) VALUES (1,'Aktiivne');
INSERT INTO t155042_Kliendi_seisundi_liik (Kliendi_seisundi_liik_kood, nimetus) VALUES (2,'Lahkunud');

INSERT INTO t155042_Laua_kategooria_tüüp (Laua_kategooria_tüüp_kood, nimetus) VALUES (100,'Paarile');
INSERT INTO t155042_Laua_kategooria_tüüp (Laua_kategooria_tüüp_kood, nimetus) VALUES (101,'Perele');
INSERT INTO t155042_Laua_kategooria_tüüp (Laua_kategooria_tüüp_kood, nimetus) VALUES (102,'Seltskonnale');
	
INSERT INTO t155042_Laua_kategooria (Laua_kategooria_kood, Laua_kategooria_tüüp_kood, nimetus) VALUES (1,100,'Väike');
INSERT INTO t155042_Laua_kategooria (Laua_kategooria_kood, Laua_kategooria_tüüp_kood, nimetus) VALUES (2,101,'Keskmine');
INSERT INTO t155042_Laua_kategooria (Laua_kategooria_kood, Laua_kategooria_tüüp_kood, nimetus) VALUES (3,102,'Suur');

INSERT INTO t155042_Laua_seisundi_liik (Laua_seisundi_liik_kood, nimetus) VALUES (1,'Ootel');
INSERT INTO t155042_Laua_seisundi_liik (Laua_seisundi_liik_kood, nimetus) VALUES (2,'Aktiivne');
INSERT INTO t155042_Laua_seisundi_liik (Laua_seisundi_liik_kood, nimetus) VALUES (3,'Mitteaktiivne');
INSERT INTO t155042_Laua_seisundi_liik (Laua_seisundi_liik_kood, nimetus) VALUES (4,'Lõpetatud');


INSERT INTO t155042_Laua_tüüp (Laua_tüüp_kood, kohtade_arv, nimetus) VALUES (1,2,'Kahene laud');
INSERT INTO t155042_Laua_tüüp (Laua_tüüp_kood, kohtade_arv, nimetus) VALUES (2,4,'Neljane laud');
INSERT INTO t155042_Laua_tüüp (Laua_tüüp_kood, kohtade_arv, nimetus) VALUES (3,10,'Kümnene laud');

INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (1,'Tööl');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (2,'Katseajal');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (3,'Puhkusel');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (4,'Lapsehoolduspuhkusel');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (5,'Haiguslehel');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (6,' Töösuhe peatatud');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (7,' Töösuhe lõpetatud töötaja omal soovil');
INSERT INTO t155042_Töötaja_seis_liik (Töötaja_seisundi_liik_kood, nimetus) VALUES (8,' Vallandatud');

commit;
INSERT INTO t155042_Töötaja (Töötaja_id, amet_kood, töötaja_seisundi_liik_kood) VALUES (16, 1, 1);
COmmit;
INSERT INTO t155042_Töötaja (Töötaja_id, amet_kood, töötaja_seisundi_liik_kood, mentor) VALUES (15, 2, 3,16);

Commit;

INSERT INTO t155042_Klient (KLIENT_KOOD, KLIENDI_SEISUNDI_LIIK_KOOD) VALUES (2, 1);
INSERT INTO t155042_Laud (Laud_kood,  Kommentaar, Laua_seisundi_liik_kood, Laua_tüüp_kood, Registreerija_kood) VALUES (10, 'Kõik oli suurepärane',2,1,16);
Commit;
INSERT INTO t155042_Laua_kat_oma (Laud_kood, Laua_kategooria_kood) VALUES (10,2);
Commit;

/*Create views */

CREATE OR REPLACE VIEW T155042_Akiivne_V_Mitte_Laud AS
SELECT 
     B.Laud_kood
    ,A.Nimetus Hekteseisnud
    ,C.nimetus Laua_tyyp
FROM t155042_laua_seisundi_liik  A
    INNER JOIN t155042_laud B ON A.laua_seisundi_liik_kood = B.laua_seisundi_liik_kood
    INNER JOIN t155042_laua_tüüp C ON B.Laua_tüüp_kood = C.Laua_tüüp_kood
WHERE A.laua_seisundi_liik_kood IN(2,3);

COMMENT ON TABLE T155042_Akiivne_V_Mitte_Laud IS 'Sisaldab informatsioon ainult aktiivsete ja mitteaktiivsete laudade kohta ';

CREATE OR REPLACE VIEW T155042_Koik_lauad AS
SELECT 
     B.Laud_kood
    ,C.nimetus Laua_tyyp
    ,A.NIMETUS Hetke_seisund
    ,B.REG_AEG
    ,CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(D.E_MEIL , ' (') ,D.eesnimi), ' '),D.perenimi),')') Registeerija
FROM t155042_laua_seisundi_liik  A
    INNER JOIN t155042_laud B ON A.laua_seisundi_liik_kood = B.laua_seisundi_liik_kood
    INNER JOIN t155042_laua_tüüp C ON B.Laua_tüüp_kood = C.Laua_tüüp_kood
    INNER JOIN t155042_isik D ON d.isik_id = B.REGISTREERIJA_KOOD;

COMMENT ON TABLE T155042_Koik_lauad IS 'Sisaldab informatsiooni laudade nimekirja kohta koos detailsete andmetega: hetkeseisund, registeerimise aeg ja registreerinud töötaja ';

CREATE OR REPLACE VIEW T155042_Laudade_Detailid AS
SELECT 
     B.Laud_kood
    ,C.nimetus Laua_tyyp
    ,A.NIMETUS Hetke_seisund
    ,CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(D.E_MEIL , ' (') ,D.eesnimi), ' '),D.perenimi),')') Registeerija
    ,B.REG_AEG
    ,B.KOMMENTAAR
FROM t155042_laua_seisundi_liik  A
    INNER JOIN t155042_laud B ON A.laua_seisundi_liik_kood = B.laua_seisundi_liik_kood
    INNER JOIN t155042_laua_tüüp C ON B.Laua_tüüp_kood = C.Laua_tüüp_kood
    INNER JOIN t155042_isik D ON d.isik_id = B.REGISTREERIJA_KOOD;

COMMENT ON TABLE T155042_Laudade_Detailid IS 'Sisaldab informatsiooni laudade nimekirja kohta koos detailsete andmetega: hetkeseisund, registeerimise aeg, registreerinud töötaja ja kommentaar ';


CREATE OR REPLACE VIEW T155042_Laudade_kat_omamine AS
SELECT 
     C.LAUD_KOOD
    ,CONCAT(CONCAT(CONCAT(B.NIMETUS, ' ('),A.Nimetus),')') Kategooria
FROM T155042_LAUA_KATEGOORIA_TÜÜP A 
    INNER JOIN T155042_LAUA_KATEGOORIA  B ON A.Laua_Kategooria_tüüp_kood = B.Laua_Kategooria_tüüp_kood
    INNER JOIN T155042_LAUA_KAT_OMA C ON B.LAUA_KATEGOORIA_KOOD = C.LAUA_KATEGOORIA_KOOD;

COMMENT ON TABLE T155042_Laudade_kat_omamine IS 'Sisaldab infot laua kategooria ja tüübi kohta';

CREATE OR REPLACE VIEW T155042_Laudade_koondaruanne AS
SELECT *
FROM (
	SELECT 
		 A.LAUA_SEISUNDI_LIIK_KOOD
		,UPPER(A.NIMETUS) nimetus
		,COUNT(*) Seisunds_Laudade_Arv
	FROM T155042_LAUA_SEISUNDI_LIIK A 
		INNER JOIN T155042_Laud B ON A.LAUA_SEISUNDI_LIIK_KOOD = B.LAUA_SEISUNDI_LIIK_KOOD
		
	GROUP BY 
		 A.NIMETUS
		,A.LAUA_SEISUNDI_LIIK_KOOD
) A 
ORDER BY Seisunds_Laudade_Arv,nimetus;

COMMENT ON TABLE T155042_Laudade_koondaruanne IS 'Sisaldab infot iga laua seisundi kohta, selle koodi, nimetust ja selles seisundis olevate laudade arvu';

COMMIT;

/* ToDo

Delete next indexes from EA ,because those indexes already existing .
CREATE INDEX t155042_IXFK_TÖÖTAJA_ISIK 
CREATE INDEX t155042_IXFK_KLIENT_ISIK  


*/
